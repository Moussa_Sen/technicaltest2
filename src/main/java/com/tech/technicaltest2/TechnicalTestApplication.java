package com.tech.technicaltest2;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class TechnicalTestApplication {


	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestApplication.class, args);
	}
}