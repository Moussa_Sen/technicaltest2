package com.tech.technicaltest2.runtime.repository;

import com.tech.technicaltest2.domain.entities.Emploi;
import com.tech.technicaltest2.domain.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Integer> {
    List<Personne> findByEmploisNomEntreprise(String nomEntreprise);

    @Query("select p from Personne p inner join Emploi e on p.id = e.personne.id where e.nomEntreprise = :nomEntreprise")
    List<Personne> findByNomEntreprise(@Param("nomEntreprise")String nomEntreprise );
}
