package com.tech.technicaltest2.runtime.repository;

import com.tech.technicaltest2.domain.entities.Emploi;
import com.tech.technicaltest2.domain.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EmploiRepository extends JpaRepository<Emploi, Integer> {

    List<Emploi> findByNomEntreprise(String name);
//    Emploi findByDateDebutBetweenAndDateFinAndPersonneNomAndPersonnePrenomAndPersonneDateNaissance(LocalDate dateDebut,LocalDate dateFin,
//                                                                                               String nom,String prenom,LocalDate dateNaissance);

    @Query("select e from Personne p inner join Emploi e on p.id = e.personne.id where (e.dateDebut >= :dateDebut and (:dateFin is null or e.dateFin <= :dateFin) ) and p.nom=:nom and p.prenom=:prenom and p.dateNaissance=:dateNaissance")
    List<Emploi> findByDurationPersonne(@Param("dateDebut")LocalDate dateDebut,@Param("dateFin")LocalDate dateFin,
            @Param("nom")String nom,@Param("prenom")String prenom,@Param("dateNaissance")LocalDate dateNaissance);


}
