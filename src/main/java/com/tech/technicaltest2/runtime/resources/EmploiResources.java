package com.tech.technicaltest2.runtime.resources;

import com.tech.technicaltest2.domain.dtos.EmploiDTO;
import com.tech.technicaltest2.domain.dtos.PlageDateDTO;
import com.tech.technicaltest2.domain.services.EmploiService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;


@RestController
@RequestMapping(value="/api/v1/emploi")
public class EmploiResources {

    private final EmploiService emploiService;

    public EmploiResources(EmploiService emploiService) {
        this.emploiService = emploiService;
    }

    @Operation(summary = "ajouter un nouveau emploi ")
    @PostMapping("")
    public EmploiDTO saveEmploi (@RequestBody @Valid EmploiDTO emploiDTO)  {
        return this.emploiService.saveEmploi(emploiDTO);
    }

    @Operation(summary = "Renvoie tous les emplois ")
    @GetMapping("")
    public List<EmploiDTO> listEmplois()  {
        return this.emploiService.EmploiDTOList();
    }

    @Operation(summary = "renvoie un emploi par son id")
    @GetMapping("/{emploiId}")
    public EmploiDTO getEmploi(@PathVariable @Min(1) Integer emploiId) {
        return this.emploiService.getEmploi(emploiId);
    }
    @Operation(summary = "Renvoie tous les emplois d'une personne entre deux plages de dates")
    @PostMapping("/plagedate")
    public List<EmploiDTO> getEmploiByDuration(@RequestBody @Valid PlageDateDTO plageDateDTO) {
        return this.emploiService.getEmploiByDuration(plageDateDTO);
    }

}
