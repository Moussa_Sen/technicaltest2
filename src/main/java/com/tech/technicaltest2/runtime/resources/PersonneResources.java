package com.tech.technicaltest2.runtime.resources;

import com.tech.technicaltest2.domain.dtos.PersonneDTO;
import com.tech.technicaltest2.domain.dtos.PersonneWithAgeDTO;
import com.tech.technicaltest2.domain.services.PersonneService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.List;


@RestController
@RequestMapping(value="/api/v1/personne")
public class PersonneResources {

    private final PersonneService personneService;

    public PersonneResources(PersonneService personneService) {
        this.personneService = personneService;
    }

    @Operation(summary = "ajouter une nouvelle personne ")
    @PostMapping("")
    public PersonneDTO savePersonne (@RequestBody @Valid PersonneDTO personneDTO)  {
        return this.personneService.savePersonne(personneDTO);
    }

    @Operation(summary = "Renvoie toutes les personnes avec leur age et emploi")
    @GetMapping("")
    public List<PersonneWithAgeDTO> listPersonnes()  {
        return this.personneService.PersonneDTOList();
    }

    @Operation(summary = "Renvoie une personne par son ref")
    @GetMapping("/{personneId}")
    public PersonneDTO getPersonne(@PathVariable @Min(1) Integer personneId) {
        return this.personneService.getPersonne(personneId);
    }
    @Operation(summary = "Renvoie toutes les personnes ayant travaillé pour une entreprise donnée.")
    @GetMapping("/{nom}")
    public List<PersonneDTO> getPersonneByCompanyName(@PathVariable @NotEmpty String nom) {
        return this.personneService.getPersonneByCompanyName(nom);
    }

}
