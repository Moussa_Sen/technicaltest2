package com.tech.technicaltest2.runtime.handlers.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ResourceException extends ResponseStatusException {

    public ResourceException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }
}