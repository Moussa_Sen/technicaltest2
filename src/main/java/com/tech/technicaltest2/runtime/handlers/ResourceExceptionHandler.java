package com.tech.technicaltest2.runtime.handlers;

import com.tech.technicaltest2.runtime.handlers.codes.ApiError;
import com.tech.technicaltest2.runtime.handlers.errors.ResourceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ResourceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ResourceException.class})
    protected ResponseEntity<Object> handleNotFound(ResourceException ex, WebRequest request) {


        return handleExceptionInternal(ex, new ApiError(HttpStatus.valueOf(ex.getRawStatusCode()),ex.getReason()),
                new HttpHeaders(), HttpStatus.valueOf(ex.getRawStatusCode()), request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), errors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }
    @ExceptionHandler({RuntimeException.class})
    protected ResponseEntity<Object> InternalError(RuntimeException ex, WebRequest request) {

        return handleExceptionInternal(ex, new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,ex.getMessage()),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
