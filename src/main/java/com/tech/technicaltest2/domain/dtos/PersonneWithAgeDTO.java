package com.tech.technicaltest2.domain.dtos;

import lombok.Builder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.List;


@Builder
public record PersonneWithAgeDTO( @NotBlank String nom, @NotBlank String prenom, @Min(13) Integer age,
                                  List<EmploiDTO> emploiDTO) {

}
