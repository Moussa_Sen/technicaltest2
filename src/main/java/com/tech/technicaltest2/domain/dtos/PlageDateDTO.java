package com.tech.technicaltest2.domain.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

//@Builder@JsonProperty("date_debut")
@Builder
public record PlageDateDTO(@NotNull @Past @JsonProperty("date_debut") LocalDate dateDebut,
                           @JsonProperty("date_fin") LocalDate dateFin,
                           @NotBlank String nom,@NotBlank String prenom,
                           @NotNull @Past @JsonProperty("date_naissance") LocalDate dateNaissance) {
}
