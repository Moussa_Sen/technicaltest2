package com.tech.technicaltest2.domain.dtos;

import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;


@Builder
public record PersonneDTO(Integer id, @NotBlank String nom, @NotBlank String prenom, @NotNull @Past LocalDate dateNaissance) {

}
