package com.tech.technicaltest2.domain.dtos;

import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Builder
public record EmploiDTO(Integer id, @NotBlank String nomEntreprise, @NotBlank String poste, @NotNull @Past LocalDate dateDebut, LocalDate dateFin) {}

