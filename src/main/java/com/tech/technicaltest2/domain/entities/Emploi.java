package com.tech.technicaltest2.domain.entities;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Emploi implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nomEntreprise;
    private String poste;
    private LocalDate dateDebut;
    private LocalDate dateFin;
    @ManyToOne()
    private Personne personne;
}
