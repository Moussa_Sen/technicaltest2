package com.tech.technicaltest2.domain.mappers;

import com.tech.technicaltest2.domain.dtos.*;
import com.tech.technicaltest2.domain.entities.*;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MyMapper {
//    @Autowired
//    protected SimpleService simpleService;


    //    @Mapping(target="startDt", source="dto.employeeStartDt", dateFormat="dd-MM-yyyy HH:mm:ss")
//    @Mapping(target = "name", expression = "java(simpleService.enrichName(source.getName()))")
    PersonneDTO toPersonneDto(final Personne personne);
    EmploiDTO toEmploiDto (final Emploi emploi);
}
