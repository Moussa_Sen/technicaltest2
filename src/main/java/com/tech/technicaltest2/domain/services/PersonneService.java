package com.tech.technicaltest2.domain.services;

import com.tech.technicaltest2.domain.dtos.PersonneDTO;
import com.tech.technicaltest2.domain.dtos.PersonneWithAgeDTO;
import com.tech.technicaltest2.domain.entities.Personne;
import com.tech.technicaltest2.domain.mappers.MyMapper;
import com.tech.technicaltest2.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest2.runtime.repository.PersonneRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.tech.technicaltest2.domain.utils.ConvertDate.calculateAge;

@Service
@Slf4j
@Transactional

public class PersonneService {

    private final PersonneRepository personneRepository;

    private final MyMapper myMapper;

    public PersonneService(PersonneRepository personneRepository, MyMapper myMapper) {
        this.personneRepository = personneRepository;
        this.myMapper=myMapper;
    }

    public  List<PersonneWithAgeDTO> PersonneDTOList() {

        List<Personne> personnes = this.personneRepository.findAll();
        if(personnes.isEmpty())
            throw new ResourceException(HttpStatus.NOT_FOUND,"Personnes non trouvées");

        return personnes.stream().map(p-> PersonneWithAgeDTO.builder()
                             .nom(p.getNom()).prenom(p.getPrenom())
                             .emploiDTO(p.getEmplois().stream().map(myMapper::toEmploiDto).collect(Collectors.toList()) )
                            .age(calculateAge(p.getDateNaissance(),LocalDate.now())).build())
                .collect(Collectors.toList());

//        return personnes.stream()
//                .map(myMapper::toPersonneDto)
//                .collect(Collectors.toList());
    }

    public PersonneDTO getPersonne(Integer personneId) {
        Optional<Personne> personne=this.personneRepository.findById(personneId);
        if(personne.isEmpty())
                throw new ResourceException(HttpStatus.NOT_FOUND,"Personne non trouvée");

        return this.myMapper.toPersonneDto(personne.get());
    }

    public PersonneDTO savePersonne(PersonneDTO personneDTO) {
        if(calculateAge(personneDTO.dateNaissance(), LocalDate.now()) >=150)
            throw new ResourceException(HttpStatus.BAD_REQUEST,"Cette personne a plus 150 ans");

        Personne personne = new Personne();
        personne.setNom(personneDTO.nom());
        personne.setPrenom(personneDTO.prenom());
        personne.setDateNaissance(personneDTO.dateNaissance());

        return this.myMapper.toPersonneDto(this.personneRepository.save(personne));
    }


    public List<PersonneDTO> getPersonneByCompanyName(String name) {
        List<Personne> personnes=this.personneRepository.findByEmploisNomEntreprise(name);
        if(personnes.isEmpty())
            throw new ResourceException(HttpStatus.NOT_FOUND,"Personne not found");

        return personnes.stream()
                .map(myMapper::toPersonneDto)
                .collect(Collectors.toList());
    }

}
