package com.tech.technicaltest2.domain.services;

import com.tech.technicaltest2.domain.dtos.EmploiDTO;
import com.tech.technicaltest2.domain.dtos.PlageDateDTO;
import com.tech.technicaltest2.domain.entities.Emploi;
import com.tech.technicaltest2.domain.mappers.MyMapper;
import com.tech.technicaltest2.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest2.runtime.repository.EmploiRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor

public class EmploiService {

    private final EmploiRepository emploiRepository;

    private final MyMapper myMapper;

    public EmploiService(EmploiRepository emploiRepository, MyMapper myMapper) {
        this.emploiRepository = emploiRepository;
        this.myMapper=myMapper;
    }

    public  List<EmploiDTO> EmploiDTOList() {

        List<Emploi> emplois = this.emploiRepository.findAll();
        if(emplois.isEmpty())
            throw new ResourceException(HttpStatus.NOT_FOUND,"Emplois non trouvés");

        return emplois.stream()
                .map(myMapper::toEmploiDto)
                .collect(Collectors.toList());

    }

    public EmploiDTO getEmploi(Integer emploiId) {
        Optional<Emploi> emploi=this.emploiRepository.findById(emploiId);
        emploi.orElseThrow(() ->  new ResourceException(HttpStatus.NOT_FOUND,"Emploi non trouvé"));

        return this.myMapper.toEmploiDto(emploi.get());
    }

    public EmploiDTO saveEmploi(EmploiDTO emploiDTO) {

//        Optional.ofNullable(emploiDTO.dateDebut()).orElseThrow(() -> new ResourceException(HttpStatus.BAD_REQUEST,"La date de debut incorrecte"));
//        if(emploiDTO.dateDebut().isAfter(LocalDate.now()))
//            throw new ResourceException(HttpStatus.BAD_REQUEST,"La date de debut incorrecte");

        if(Optional.ofNullable(emploiDTO.dateFin()).isPresent())
            if(emploiDTO.dateDebut().isAfter(emploiDTO.dateFin()))
                throw new ResourceException(HttpStatus.BAD_REQUEST,"La date de debut doit commencer avant la date de fin");

        var emploi = Emploi.builder()
                                .nomEntreprise(emploiDTO.nomEntreprise())
                                .poste(emploiDTO.poste())
                                .dateDebut(emploiDTO.dateDebut())
                                .dateFin(emploiDTO.dateFin()).build();


        return this.myMapper.toEmploiDto(this.emploiRepository.save(emploi));

    }

    public List<EmploiDTO> getEmploiByDuration(PlageDateDTO plageDate) {
        if(Optional.ofNullable(plageDate.dateFin()).isPresent())
            if(plageDate.dateDebut().isAfter(plageDate.dateFin()))
                throw new ResourceException(HttpStatus.BAD_REQUEST,"La date de debut doit commencer avant la date de fin");


        List<Emploi> emplois=this.emploiRepository.findByDurationPersonne(plageDate.dateDebut(),plageDate.dateFin(),plageDate.nom(),plageDate.prenom(),plageDate.dateNaissance());
        if(emplois.isEmpty())
            throw new ResourceException(HttpStatus.NOT_FOUND,"Emploi non trouvé");

        return emplois.stream()
                .map(myMapper::toEmploiDto)
                .collect(Collectors.toList());

    }

}
