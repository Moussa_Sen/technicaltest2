INSERT INTO PERSONNE (id, prenom,nom,date_naissance) VALUES (1, 'Bernard','Fleur','1986-02-03');
INSERT INTO PERSONNE (id, prenom,nom,date_naissance) VALUES (2, 'Lucas','Gomez','1996-06-03');
INSERT INTO PERSONNE (id, prenom,nom,date_naissance) VALUES (3, 'Rita','Bendelladj','2002-03-06');
INSERT INTO EMPLOI (id,nom_entreprise,poste,date_debut,date_fin,personne_id) VALUES (1,'Apple','dev fullStack','2018-06-03','2022-06-03',1);
INSERT INTO EMPLOI (id,nom_entreprise, poste,date_debut,date_fin,personne_id) VALUES (2,'web-atrio', 'Architect','2015-05-03','2019-06-03',1);
INSERT INTO EMPLOI (id,nom_entreprise, poste,date_debut,date_fin,personne_id) VALUES (3, 'X','Lead dev','2013-06-03','2015-06-03',1);
INSERT INTO EMPLOI (id,nom_entreprise, poste,date_debut,date_fin,personne_id) VALUES (4,'Microsoft','dev','2012-06-14','2020-06-03',2);
INSERT INTO EMPLOI (id,nom_entreprise, poste,date_debut,date_fin,personne_id) VALUES (5,'web-atrio','Scrum Master','2002-06-03','2012-06-02',2);
INSERT INTO EMPLOI (id,nom_entreprise, poste,date_debut,date_fin,personne_id) VALUES (6,'Bizao', 'Devops','2020-06-03',null,3);