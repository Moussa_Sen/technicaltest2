package com.tech.technicaltest2.servicestest;

import com.tech.technicaltest2.domain.dtos.EmploiDTO;
import com.tech.technicaltest2.domain.dtos.PersonneDTO;
import com.tech.technicaltest2.domain.dtos.PlageDateDTO;
import com.tech.technicaltest2.domain.services.EmploiService;
import com.tech.technicaltest2.domain.services.PersonneService;
import com.tech.technicaltest2.runtime.handlers.errors.ResourceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=RANDOM_PORT)
public class TechTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private EmploiService emploiService;

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test(expected = ResourceException.class)
    public void should_returnException(){
        assertThat(personneService.getPersonne(25)).isEqualTo(PersonneDTO.builder().id(1).nom("Matilde").build());

    }
    public void should_getPersonne(){
        assertThat(personneService.getPersonne(1)).isEqualTo(PersonneDTO.builder().id(1).nom("Bernard").build());
    }
    @Test
    public void should_getPersonneByCompanyName(){
        var pers =List.of(PersonneDTO.builder().id(1).prenom("Bernard").nom("Fleur").dateNaissance(LocalDate.of(1986,2,3)).build(),
                PersonneDTO.builder().id(2).prenom("Lucas").nom("Gomez")
                .dateNaissance(LocalDate.of(1996,6,3)).build());
        assertThat(personneService.getPersonneByCompanyName("web-atrio")).isEqualTo(pers);

    }
    @Test
    public void should_getEmploiByDuration(){

        var plageDateDto= PlageDateDTO.builder().dateDebut(LocalDate.of(2013,6,3))
                .dateFin(LocalDate.of(2019,6,3)).prenom("Bernard").nom("Fleur").dateNaissance(LocalDate.of(1986,2,3)).build();
        var emplois =List.of(EmploiDTO.builder().id(2).nomEntreprise("web-atrio").poste("Architect")
                        .dateDebut(LocalDate.of(2015,5,3))
                                .dateFin(LocalDate.of(2019,6,3)).build(),
                EmploiDTO.builder().id(3).nomEntreprise("X").poste("Lead dev")
                        .dateDebut(LocalDate.of(2013,6,3))
                        .dateFin(LocalDate.of(2015,6,3)).build());

        assertThat(emploiService.getEmploiByDuration(plageDateDto)).isEqualTo(emplois);

    }
    @Test
    public void should_getEmploi(){
        assertThat(emploiService.getEmploi(2))
                .isEqualTo(
                        EmploiDTO.builder().id(2).nomEntreprise("web-atrio").poste("Architect")
                                .dateDebut(LocalDate.of(2015,5,3)).dateFin(LocalDate.of(2019,6,3)).build());
    }
    @Test
    public void should_getPersonneSize(){
        assertThat(personneService.PersonneDTOList().size()).isEqualTo(3);

    }
    @Test
    public void should_getEmploiSize(){
        assertThat(emploiService.EmploiDTOList().size()).isEqualTo(6);
    }

    @Test(expected = ResourceException.class)
    public void should_returnErrorDatedebutEmploiIncorrecte() {

        EmploiDTO emploiDTO = EmploiDTO.builder()
                .nomEntreprise("SpaceX")
                .poste("Architect")
                .dateDebut(LocalDate.of(2021,8,14))
                .dateFin(LocalDate.of(2020,8,14))
                .build();


        assertThat(emploiService.saveEmploi(emploiDTO)).isNotNull();
    }
    @Test()
    public void validationWhenNoDateDebutEmploiThenThrowException() {
        EmploiDTO emploiDTO = EmploiDTO.builder()
                .nomEntreprise("Solution")
                .poste("Architect")
                .dateDebut(null)
                .dateFin(LocalDate.of(2020,8,14))
                .build();

        Set<ConstraintViolation<EmploiDTO>> violations = this.validator.validate(emploiDTO);
        assertFalse(violations.isEmpty());
    }

    @Test()
    public void should_createEmploiwithoutEndate() {

        EmploiDTO emploiDTO = EmploiDTO.builder()
                .nomEntreprise("Atos")
                .poste("Architect")
                .dateDebut(LocalDate.of(2023,4,14))
                .dateFin(null)
                .build();

        assertThat(emploiService.saveEmploi(emploiDTO)).isNotNull();
    }

    @Test()
    public void should_createPersonne() {

        PersonneDTO pers = PersonneDTO.builder()
                .id(5)
                .prenom("Sarah")
                .nom("Raush")
                .dateNaissance(LocalDate.of(1965,5,8))
                .build();

        assertThat(personneService.savePersonne(pers)).isNotNull();
    }
    @Test
    public void should_catchErrorPersonneDateNaissance() {

        PersonneDTO pers = PersonneDTO.builder()
                .prenom("Sarah")
                .nom("Raush")
                .dateNaissance(LocalDate.of(2026,5,8))
                .build();
        Set<ConstraintViolation<PersonneDTO>> violations = this.validator.validate(pers);
        assertFalse(violations.isEmpty());
    }
    @Test(expected = ResourceException.class)
    public void should_catchErrorPersonneDateNaissanceSup150() {

        PersonneDTO pers = PersonneDTO.builder()
                .id(4)
                .prenom("Sarah")
                .nom("Raush")
                .dateNaissance(LocalDate.of(1820,5,8))
                .build();

        assertThat(personneService.savePersonne(pers)).isEqualTo(pers);
    }
    @Test
    public void should_createPersonneRest() {

        PersonneDTO cli = PersonneDTO.builder()
                .id(5)
                .prenom("Sarah")
                .nom("Raush")
                .dateNaissance(LocalDate.of(1994,5,8))
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<PersonneDTO> request = new HttpEntity<>(cli, headers);

        ResponseEntity<PersonneDTO> response =
                restTemplate.postForEntity("/api/v1/personne",request ,PersonneDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().nom()).isEqualTo("Raush");
    }

    @Test
    public void should_createEmploi() {

        EmploiDTO emploiDTO = EmploiDTO.builder()
                .poste("Chef de projet")
                .nomEntreprise("Atos")
                .dateDebut(LocalDate.of(2002,8,14))
                .dateFin(LocalDate.of(2022,8,14))
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<EmploiDTO> request = new HttpEntity<>(emploiDTO, headers);

        ResponseEntity<EmploiDTO> response =
                restTemplate.postForEntity("/api/v1/emploi",request ,EmploiDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().poste()).isEqualTo("Chef de projet");
    }
}
