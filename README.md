# Welcome To the jungle
## Fonctionnalités
J'ai conçu et développé une application web en utilisant le framework Java Spring. 
Cela inclut la configuration de l'application, la création de contrôleurs,
la gestion des dépendances et la mise en place de l'accès aux données. 
L'application est désormais opérationnelle, permettant aux utilisateurs de :
- sauvegarder une nouvelle personne. Attention, seules les personnes de moins de 150 ans peuvent être enregistrées.
Sinon, renvoie une erreur.
- permettre d'ajouter un emploi à une personne avec une date de début et de fin d'emploi.
Pour le poste actuellement occupé, la date de fin n'est pas obligatoire. 
- une personne peut avoir plusieurs emplois aux dates qui se chevauchent.
- renvoyer toutes les personnes enregistrées par ordre alphabétique, 
et indiquent également leur âge et leur(s) emploi(s) actuel(s).
- renvoyer toutes les personnes ayant travaillé pour une entreprise donnée.
- renvoyer tous les emplois d'une personne entre deux plages de dates.
## Test
Pour tester les fonctionnalités:
- vous pouvez récupérer l'API DOC
[en cliquant ici](src/main/resources/api-docs.json).
- vous pouvez lancer les tests unitaires depuis la classe [TechTest](src/test/java/com/tech/technicaltest2/servicestest/TechTest.java).
## Environnement Technique
#java jdk17 #spring #spring boot #spring Data JPA #H2 #SpringDoc #Api REST #Lombok #MapStruct #Junit #Docker
